package com.coding.katagame.engine;

/**
 * katagame
 * Copyright (C) 2020  Fabio Cantarini <fabio.cantarini@libero.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
public class EngineResult {
	
	public enum OutcomeEnum {
		SUCCESS,
		FAILURE
	}
	
	protected OutcomeEnum outcome;
	protected Player targetPlayer;
	
	protected EngineResult(OutcomeEnum outcome, Player targetPlayer) {
		this.outcome = outcome;
		this.targetPlayer = targetPlayer;
	}
	
	public Player getTargetPlayer() {
		return targetPlayer;
	}

	public OutcomeEnum getOutcome() {
		return outcome;
	}

	public boolean succeeded() {
		return OutcomeEnum.SUCCESS.equals(outcome);
	}
	
}
