package com.coding.katagame.engine;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * katagame
 * Copyright (C) 2020  Fabio Cantarini <fabio.cantarini@libero.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
public class Engine {

	private Map<String, Player> players = new HashMap<>();
	
	public void start() {
		players.clear();
	}

	public AddPlayerResult addPlayer(String name) {
		Player player = players.get(name);
		if (player != null) {
			return AddPlayerResult.failureExistingPlayer(player);
		}
		player = new Player(name);
		players.put(player.getName(), player);
		return AddPlayerResult.success(player);
	}

	public MovePlayerResult move(String name, int roll1, int roll2) {
		Player player = this.players.get(name);
		if (player == null) {
			return MovePlayerResult.failureNonExistingPlayer(name);
		}
		MovePlayerResult result = MovePlayerResult.success(player, player.getSpace());
		return move(player, roll1, roll2, result);
	}

	private MovePlayerResult move(Player player, int roll1, int roll2, MovePlayerResult result) {
		Space fromSpace = player.getSpace();
		Space toSpace = fromSpace.move(roll1, roll2);

		// The Bridge
		if (toSpace.isTheBridge()) {
			result.addStep(toSpace);
			toSpace = Space.newSpace(2 * toSpace.getNumber());
		}
		
		// The Goose
		while (toSpace.isTheGoose()) {
			result.addStep(toSpace);
			toSpace = toSpace.move(roll1, roll2);
		}

		// Bounced
		if (toSpace.isBounced()) {
			result.addStep(toSpace);
			toSpace = Space.newSpace(toSpace.getBouncedNumber());
		}
		
		result.addStep(toSpace);
		player.setSpace(toSpace);
		
		// Prank
		getPlayers().filter(p -> p != player && p.sameSpace(player)).forEach(p -> prank(result, p, fromSpace));
		return result;
	}

	private void prank(MovePlayerResult result, Player player, Space prevSpace) {
		result.addPrankPlayer(player);
		player.setSpace(prevSpace);
	}
	
	public boolean hasPlayers() {
		return !this.players.isEmpty();
	}
	
	public Stream<Player> getPlayers() {
		return players.values().stream();
	}
}
