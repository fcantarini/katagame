package com.coding.katagame.engine;

import java.util.ArrayList;
import java.util.List;

/**
 * katagame
 * Copyright (C) 2020  Fabio Cantarini <fabio.cantarini@libero.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
public class MovePlayerResult extends EngineResult {
	
	public enum FailureEnum {
		NON_EXISTING_PLAYER,
		NONE
	}
	
	private FailureEnum failure;
	private Space fromSpace;
	private List<Space> steps = new ArrayList<>();
	private List<Player> prankPlayers = new ArrayList<>();

	protected MovePlayerResult(OutcomeEnum outcome, FailureEnum failure, Player targetPlayer, Space fromSpace) {
		super(outcome, targetPlayer);
		this.failure = failure;
		this.fromSpace = fromSpace;
	}
	
	public static MovePlayerResult failureNonExistingPlayer(String playerName) {
		Player player = new Player(playerName);
		MovePlayerResult result = new MovePlayerResult(OutcomeEnum.FAILURE, FailureEnum.NON_EXISTING_PLAYER, player, player.getSpace());
		return result;
	}
	
	public static MovePlayerResult success(Player player, Space fromSpace) {
		MovePlayerResult result = new MovePlayerResult(OutcomeEnum.SUCCESS, FailureEnum.NONE, player, fromSpace);
		return result;
	}

	public FailureEnum getFailure() {
		return failure;
	}
	
	public void addStep(Space space) {
		this.steps.add(space);
	}
	
	public List<Space> getSteps() {
		return this.steps;
	}

	public Space getFromSpace() {
		return this.fromSpace;
	}
	
	public void addPrankPlayer(Player player) {
		this.prankPlayers.add(player);
	}
	
	public List<Player> getPrankPlayers() {
		return this.prankPlayers;
	}
}
