package com.coding.katagame.engine;

/**
 * katagame
 * Copyright (C) 2020  Fabio Cantarini <fabio.cantarini@libero.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
public class Player {

	private String name;
	private Space space;
		
	public Player(String name) {
		this.name = name;
		this.space = new Space();
	}

	public String getName() {
		return this.name;
	}

	public Space getSpace() {
		return this.space;
	}

	public void setSpace(Space space) {
		this.space = space;
	}
	
	public boolean sameSpace(Player player) {
		return this.space.sameSpace(player.space);
	}
}
