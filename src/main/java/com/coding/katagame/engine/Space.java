package com.coding.katagame.engine;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * katagame
 * Copyright (C) 2020  Fabio Cantarini <fabio.cantarini@libero.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
public class Space {

	private int number;
	private boolean bounced = false;
	private int bouncedNumber = 0;
	
	private static int START_SPACE = 0;
	private static int BRIDGE_SPACE = 6;
	private static int WINNER_SPACE = 63;
	private static Set<Integer> GOOSE_SPACES = new HashSet<>(Arrays.asList(5, 9, 14, 18, 23, 27));
	
	public Space() {
		this.number = START_SPACE;
	}

	public static Space newSpace(int number) {
		if (number < 0) {
			String msg = String.format("Space: invalid number %d", number);
			throw new IllegalArgumentException(msg);
		}
		Space space = new Space();
		if (number > WINNER_SPACE) {
			space.number = WINNER_SPACE;
			space.bounced = true;
			space.bouncedNumber = 2 * WINNER_SPACE - number;
		} else {
			space.number = number;
		}
		return space;
	}
	
	public int getNumber() {
		return this.number;
	}
	
	public int getBouncedNumber() {
		return this.bouncedNumber;
	}
	
	public boolean sameSpace(Space space) {
		return this.number == space.number;
	}
	
	public boolean isBounced() {
		return this.bounced;
	}
	
	public Space move(int roll1, int roll2) {
		int newNumber = this.number + roll1 + roll2;
		return newSpace(newNumber); 
	}
	
	public boolean isStart() {
		return this.number == START_SPACE;
	}
	
	public boolean isWinner() {
		return this.number == WINNER_SPACE;
	}
	
	public boolean isTheBridge() {
		return this.number == BRIDGE_SPACE;
	}

	public boolean isTheGoose() {
		return GOOSE_SPACES.contains(this.number);
	}
}
