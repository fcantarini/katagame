package com.coding.katagame.cli;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.coding.katagame.engine.Engine;

/**
 * katagame
 * Copyright (C) 2020  Fabio Cantarini <fabio.cantarini@libero.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
public abstract class CliOption {
	private Pattern pattern;
	private Matcher matcher;
	
	protected CliOption(String regex) {
		this.pattern = Pattern.compile(regex);
	}

	public boolean matches(String cmd) {
		this.matcher = this.pattern.matcher(cmd);
		return this.matcher.matches();
	}

	protected String getGroup(int num) {
		if (this.matcher == null) {
			throw new IllegalStateException("Input command not parsed");
		}
		return this.matcher.group(num);
	}

	public abstract boolean canApply(Engine engine);
	
	public abstract String apply(Engine engine);
	
	public abstract String synopsis();
}
