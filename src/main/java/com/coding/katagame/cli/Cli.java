package com.coding.katagame.cli;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Optional;
import java.util.Scanner;

import com.coding.katagame.engine.Engine;

/**
 * katagame
 * Copyright (C) 2020  Fabio Cantarini <fabio.cantarini@libero.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
public class Cli {

	private CliOptions options = new CliOptions();
	private PrintStream out;
	private InputStream in;
	private Scanner scanner = null;
	
	public Cli(InputStream in, PrintStream out) {
		this.in = in;
		this.out = out;
	}
	
	public void run(Engine engine) {
		engine.start();
		welcome();
		while (true) {
			options(engine);
			try {
				String cmd = input();
				Optional<CliOption> option = options.stream().filter(o -> o.matches(cmd)).findFirst();
				if (option.isPresent()) {
					CliOption cliOption = option.get();
					String outcome = cliOption.apply(engine);
					output(outcome);
				} else {
					output("Sorry, unknown command...");
				}
			} catch (Exception ex) {
				output(ex.getMessage());
				output("Bye.");
				break;
			}
		}
	}
	
	private void welcome() {
		output("Welcome to The Goose Game Kata!");
	}

	private void options(Engine engine) {
		output("-------------------");
		output("You can:");
		options.stream().filter(o -> o.canApply(engine)).map(CliOption::synopsis).forEach(this::output);
		output("-------------------");
		out.print("> ");
	}

	private void output(String msg, Object... args) {
		out.println(String.format(msg, args));
	}

	private String input() {
		if (scanner == null) {
			scanner = new Scanner(in);
		}
		return scanner.nextLine();
	}
}
