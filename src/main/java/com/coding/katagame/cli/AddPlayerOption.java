package com.coding.katagame.cli;

import java.util.stream.Collectors;

import com.coding.katagame.engine.AddPlayerResult;
import com.coding.katagame.engine.Engine;
import com.coding.katagame.engine.Player;

/**
 * katagame
 * Copyright (C) 2020  Fabio Cantarini <fabio.cantarini@libero.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
public class AddPlayerOption extends CliOption {
	
	public AddPlayerOption() {
		super("add player (\\w+)");
	}

	public String getPlayerName() {
		return getGroup(1);
	}

	@Override
	public String apply(Engine engine) {
		String player = getPlayerName();
		AddPlayerResult result = engine.addPlayer(player);
		if (result.succeeded()) {
			String players = engine.getPlayers().map(Player::getName).collect(Collectors.joining(", "));
			return String.format("players: %s", players);
		} else {
			switch(result.getFailure()) {
			case EXISTING_PLAYER:
				return String.format("%s: already existing player", player);
			default:
				return String.format("%s: cannot add player", player);
			}
		}
	}
	
	@Override
	public String synopsis() {
		return "add player <name>";
	}
	
	@Override
	public boolean canApply(Engine engine) {
		return true;
	}

}
