package com.coding.katagame.cli;

import java.util.Random;

/**
 * katagame
 * Copyright (C) 2020  Fabio Cantarini <fabio.cantarini@libero.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
public class MovePlayerRandomOption extends MovePlayerOption {

	public MovePlayerRandomOption() {
		super("move (\\w+)");
	}

	@Override
	public int getRoll1() {
		return getRoll();
	}

	@Override
	public int getRoll2() {
		return getRoll();
	}

	private int getRoll() {
		return new Random().nextInt(5) + 1;
	}
	
	@Override
	public String synopsis() {
		return "move <name>";
	}
}
