package com.coding.katagame.cli;

import com.coding.katagame.engine.Engine;
import com.coding.katagame.engine.MovePlayerResult;
import com.coding.katagame.engine.Player;
import com.coding.katagame.engine.Space;

/**
 * katagame
 * Copyright (C) 2020  Fabio Cantarini <fabio.cantarini@libero.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
public class MovePlayerOption extends CliOption {

	public MovePlayerOption() {
		super("move (\\w+) ([1-6]), ([1-6])");
	}

	protected MovePlayerOption(String regex) {
		super(regex);
	}

	public String getPlayerName() {
		return getGroup(1);
	}

	public int getRoll1() {
		return Integer.parseInt(getGroup(2));
	}

	public int getRoll2() {
		return Integer.parseInt(getGroup(3));
	}

	@Override
	public String apply(Engine engine) {
		String player = getPlayerName();
		int roll1 = getRoll1();
		int roll2 = getRoll2();
		MovePlayerResult result = engine.move(player, roll1, roll2);
		if (result.succeeded()) {
			return outcome(result, roll1, roll2);
		} else {
			switch (result.getFailure()) {
			case NON_EXISTING_PLAYER:
				return String.format("%s: non existing player", player);
			default:
				return String.format("%s: cannot move player with %d, %d", player, roll1, roll2);
			}
		}
	}

	private String outcome(MovePlayerResult result, int roll1, int roll2) {
		StringBuilder builder = new StringBuilder();
		Player player = result.getTargetPlayer();
		Space fromSpace = result.getFromSpace();
		builder.append(String.format("%s rolls %d, %d", player.getName(), roll1, roll2));
		Space prevStep = null;
		for (Space step : result.getSteps()) {
			if (prevStep == null) {
				builder.append(String.format(". %s moves from %s to %s", player.getName(), getDescription(fromSpace), getDescription(step)));
			} else if (prevStep.isTheBridge()) {
				builder.append(String.format(". %s jumps to %s", player.getName(), getDescription(step)));
			} else if (prevStep.isTheGoose()) {
				builder.append(String.format(". %s moves again and goes to %s", player.getName(), getDescription(step)));
			} else if (prevStep.isBounced()) {
				builder.append(String.format(". %s bounces! %s returns to %s", player.getName(), player.getName(), getDescription(step)));
			}
			prevStep = step;
		}
		if (player.getSpace().isWinner()) {
			builder.append(String.format(". %s Wins!!", player.getName()));
		}
		for (Player prankPlayer : result.getPrankPlayers()) {
			builder.append(String.format(". On %s there is %s, who returns to %s", getDescription(player.getSpace()), prankPlayer.getName(), getDescription(fromSpace)));
		}
		return builder.toString();
	}

	private String getDescription(Space space) {
		if (space.isStart()) {
			return "Start";
		} else if (space.isTheBridge()) {
			return "The Bridge";
		} else if (space.isTheGoose()) {
			return String.format("%d, The Goose", space.getNumber());
		} else {
			return Integer.toString(space.getNumber());
		}
	}

	@Override
	public String synopsis() {
		return "move <name> <1..6>, <1..6>";
	}

	@Override
	public boolean canApply(Engine engine) {
		return engine.hasPlayers();
	}
}
