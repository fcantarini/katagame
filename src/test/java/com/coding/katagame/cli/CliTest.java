package com.coding.katagame.cli;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.coding.katagame.engine.Engine;

/**
 * katagame
 * Copyright (C) 2020  Fabio Cantarini <fabio.cantarini@libero.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
public class CliTest {

	private PrintStream in;
	private ByteArrayOutputStream out;

	@BeforeEach
	public void init() throws IOException {
		PipedOutputStream pipeOut = new PipedOutputStream();
		this.in = new PrintStream(pipeOut);
		this.out = new ByteArrayOutputStream();

		PipedInputStream cliIn = new PipedInputStream(pipeOut);
		PrintStream cliOut = new PrintStream(this.out);
		Cli cli = new Cli(cliIn, cliOut);
		Engine engine = new Engine();
		ExecutorService executor = Executors.newSingleThreadExecutor();
		executor.submit(() -> {
			cli.run(engine);
		});
	}

	private void sendToCli(String cmd) {
		try {
			this.in.println(cmd);
			this.in.flush();
			Thread.sleep(100);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void checkFromCli(String outcome) {
		String output = this.out.toString();
		Assertions.assertTrue(output.contains(outcome));
	}

	@Test
	public void test_AddPlayer() {
		sendToCli("add player Pippo");
		checkFromCli("players: Pippo");

		sendToCli("add player Pluto");
		checkFromCli("players: Pippo, Pluto");
	}

	@Test
	public void test_DuplicatedPlayer() {
		sendToCli("add player Pippo");
		checkFromCli("players: Pippo");

		sendToCli("add player Pippo");
		checkFromCli("Pippo: already existing player");
	}

	@Test
	public void test_MovePlayer() {
		sendToCli("add player Pippo");
		sendToCli("add player Pluto");

		sendToCli("move Pippo 4, 3");
		checkFromCli("Pippo rolls 4, 3. Pippo moves from Start to 7");

		sendToCli("move Pluto 2, 2");
		checkFromCli("Pluto rolls 2, 2. Pluto moves from Start to 4");

		sendToCli("move Pippo 2, 3");
		checkFromCli("Pippo rolls 2, 3. Pippo moves from 7 to 12");
	}

	@Test
	public void test_Win() {
		sendToCli("add player Pippo");

		sendToCli("move Pippo 6, 6");
		sendToCli("move Pippo 6, 6");
		sendToCli("move Pippo 6, 6");
		sendToCli("move Pippo 6, 6");
		sendToCli("move Pippo 6, 6");

		sendToCli("move Pippo 1, 2");
		checkFromCli("Pippo rolls 1, 2. Pippo moves from 60 to 63. Pippo Wins!!");
	}

	@Test
	public void test_TheGameThrowsTheDice() {
		sendToCli("add player Pippo");

		sendToCli("move Pippo 1, 3");

		sendToCli("move Pippo");
		checkFromCli("Pippo moves from 4 to ");
	}

	@Test
	public void test_TheBridge() {
		sendToCli("add player Pippo");
		sendToCli("add player Pluto");

		sendToCli("move Pippo 4, 2");
		checkFromCli("Pippo rolls 4, 2. Pippo moves from Start to The Bridge. Pippo jumps to 12");
	}

	@Test
	public void test_TheGoose() {
		sendToCli("add player Pippo");
		sendToCli("move Pippo 1, 2");

		sendToCli("move Pippo 1, 1");
		checkFromCli("Pippo rolls 1, 1. Pippo moves from 3 to 5, The Goose. Pippo moves again and goes to 7");
	}

	@Test
	public void test_TheGooseMultipleJump() {
		sendToCli("add player Pippo");
		sendToCli("move Pippo 6, 4");

		sendToCli("move Pippo 2, 2");
		checkFromCli(
				"Pippo rolls 2, 2. Pippo moves from 10 to 14, The Goose. Pippo moves again and goes to 18, The Goose. Pippo moves again and goes to 22");
	}

	@Test
	public void test_Prank() {
		sendToCli("add player Pippo");
		sendToCli("add player Pluto");
		sendToCli("move Pippo 6, 6");
		sendToCli("move Pippo 1, 2");
		sendToCli("move Pluto 6, 6");
		sendToCli("move Pluto 3, 2");

		sendToCli("move Pippo 1, 1");
		checkFromCli("Pippo rolls 1, 1. Pippo moves from 15 to 17. On 17 there is Pluto, who returns to 15");
	}
}
