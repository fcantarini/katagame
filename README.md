# katagame

Implementation for The Goose Game Kata.

https://github.com/xpeppers/goose-game-kata

# How to build

To build, run unit tests and install to target folder with Maven run:

> mvn clean install

# Play

To play the game from console:

> java -jar target/katagame-0.0.1-SNAPSHOT.jar 

> Welcome to The Goose Game Kata!


Enjoy!
